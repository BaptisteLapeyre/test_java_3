package etatPassager;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//import EtatPassagerMonter.Etat;

class EtatPassagerMonterTest {
	EtatPassager passager;

	
	@Before
	void setUp() throws Exception {
		passager = new EtatPassager(EtatPassager.Etat.DEBOUT);
		
	}

	@After
	void tearDown() throws Exception {
	}
	
	@Test
	void testEstAssis() {
		assertFalse("est debout",passager.estDebout());
		assertTrue("est assis",passager.estAssis());
		
	}
	
	@Test
	void testEstDebout() {
		assertTrue("est debout",passager.estDebout());
		assertFalse("est assis",passager.estAssis());
		
	}
		

}
