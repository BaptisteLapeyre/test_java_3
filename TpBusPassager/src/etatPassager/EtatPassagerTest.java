package etatPassager;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EtatPassagerTest {
	
	EtatPassager p, p1, p2;

	@Before
	public void setUp() throws Exception {
		p = new EtatPassager(EtatPassager.Etat.DEHORS);
		p1 = new EtatPassager(EtatPassager.Etat.ASSIS);
		p2 = new EtatPassager(EtatPassager.Etat.DEBOUT);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExterieur() {
		assertTrue( p.estExterieur());
		assertFalse( p.estAssis());
		assertFalse( p.estDebout());
	}
	
	@Test
	public void testEstAssis() {
		assertFalse( p1.estExterieur());
		assertTrue( p1.estAssis());
		assertFalse( p1.estDebout());
	}

	@Test
	public void testEstDebout() {
		assertFalse( p2.estExterieur());
		assertFalse( p2.estAssis());
		assertTrue( p2.estDebout());
	}
	
	@Test
	public void testEstInterieur() {
		assertFalse( p.estInterieur());
		assertTrue( p1.estInterieur());
		assertTrue( p2.estInterieur());
	}


	
}
