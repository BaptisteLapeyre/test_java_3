package etatPassager;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * 
 * @author Roudier
 *	Classe de test pour EtatPassager qui test avec l'implement de IEtatPessager
 */
public class IEtatPassagerTest {

	IEtatPassager p, p1, p2;

	@Before
	public void setUp() throws Exception {
		p = creerAssis("chaine");
		p1 = creerDebout("monter");
		p2 = creerDehors("normal");
	}

	@After
	public void tearDown() throws Exception {
	}

	
	/**
	 * 
	 * Test si il est � l'int�rieur
	 */
	@Test
	public void testExterieur() {

		//Pour le normal
		assertTrue( p2.estExterieur());
		assertFalse( p2.estAssis());
		assertFalse( p2.estDebout());
		
	}
	
	
	/*
	 * Test si il est Assis
	 */
	@Test
	public void testEstAssis() {
		//Pour la chaine
		assertFalse( p.estDebout());
		assertTrue(p.estAssis());
		
	}

	
	/**
	 * Test si il est debout
	 */
	@Test
	public void testEstDebout() {
		assertFalse( p1.estAssis());
		assertTrue( p1.estDebout());
	}
	
	/**
	 * Test pour savoir si c'est � l'int�rieur
	 */
	@Test
	public void testEstInterieur() {
		assertTrue( p.estInterieur());
		assertFalse( p2.estInterieur());
	}
	
	/**
	 * 
	 * On essaie de savoir quel type d'etat de passager cela est pour assis
	 * @param typePassager
	 * @return IEtatPassager
	 */
	public IEtatPassager creerAssis(String typePassager) {
		IEtatPassager passager = null;
		switch (typePassager) {
		case "chaine":
			 passager = new EtatPassagerChaine("assis");
			break;
		case "monter":
			 passager = new EtatPassagerMonter(EtatPassagerMonter.Etat.ASSIS);
			break;
		case "normal":
			passager = new EtatPassager(EtatPassager.Etat.ASSIS);
			break;
		default:
			System.out.println("Erreur de choix");
			break;
		}
	
		return passager;
	}
	
	/**
	 * 
	 * On essaie de savoir quel type d'etat de passager cela est pour debout
	 * @param typePassager
	 * @return IEtatPassager
	 */
	
	public IEtatPassager creerDebout(String typePassager) {
		IEtatPassager passager = null;
		switch (typePassager) {
		case "chaine":
			 passager = new EtatPassagerChaine("debout");
			break;
		case "monter":
			 passager = new EtatPassagerMonter(EtatPassagerMonter.Etat.DEBOUT);
			break;
		case "normal":
			passager = new EtatPassager(EtatPassager.Etat.DEBOUT);
			break;
		default:
			System.out.println("Erreur de choix");
			break;
		}
	
		return passager;
	}
	
	
	
	/**
	 * 
	 * On essaie de savoir quel type d'etat de passager cela est pour dehors
	 * @param typePassager
	 * @return IEtatPassager
	 */
	public IEtatPassager creerDehors(String typePassager) {
		
		IEtatPassager passager = null;
		switch (typePassager) {
		case "chaine":
			 passager = new EtatPassagerChaine("dehors");
			break;
		case "monter":
			 System.out.println("Vous etes deja � l'int�rieur");
			break;
		case "normal":
			passager = new EtatPassager(EtatPassager.Etat.DEHORS);
			break;
		default:
			System.out.println("Erreur de choix");
			break;
		}
		
		return passager;
	}

}
