package etatPassager;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

class EtatPassagerChaineTest {
	EtatPassagerChaine passager;
	@Before
	void setUp() throws Exception {
		passager = new EtatPassagerChaine("debout");
	}

	@After
	void tearDown() throws Exception {
	}

	@Test
	void testEstAssis() {
		assertFalse("est � l'exterieur",passager.estExterieur());
		assertFalse("est debout",passager.estDebout());
		assertTrue("est assis",passager.estAssis());
		
	}
	
	@Test
	void testEstDebout() {
		assertFalse("est � l'exterieur",passager.estExterieur());
		assertTrue("est debout",passager.estDebout());
		assertFalse("est assis",passager.estAssis());
	}
	
	@Test
	void testEstExterieur() {
		assertTrue("est � l'exterieur",passager.estExterieur());
		assertFalse("est debout",passager.estDebout());
		assertFalse("est assis",passager.estAssis());
	}
	
	@Test
	void testEstInterieur() {
		assertFalse("est � l'exterieur",passager.estExterieur());
		assertTrue("est debout",passager.estDebout());
		assertTrue("est assis",passager.estAssis());
	}

}
