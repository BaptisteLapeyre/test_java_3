package tec;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PassagerStandardTest {
	
	PassagerStandard unPS;
	Autobus b1;
	Autobus b2;
	Autobus b3;
	Autobus b4;
	Autobus b5;
	
	 PassagerStandard p1;
	 PassagerStandard p2;
	 PassagerStandard p3;
	 PassagerStandard p4;


	@Before
	public void setUp() throws Exception {
		
		 b1 = new Autobus(1, 2);
	     b2 = new Autobus(0, 0);
	     b3 = new Autobus(0, 1);
	     b4 = new Autobus(1, 0);
	     b5 = new Autobus(25, 25);
	     
	     p1 = new PassagerStandard("Michel", 1);
	     p2 = new PassagerStandard("Bernadette", 2);
	     p3 = new PassagerStandard("Hector", 3);
	     p4 = new PassagerStandard("Flanders", 5);

	}

	@After
	public void tearDown() throws Exception {
	}
	

	@Test
	public void testExterieur() {
		p1.accepterSortie();
		assertTrue(p1.estDehors());
		
		p2.accepterPlaceAssise();
		assertFalse(p2.estDehors());
		
		p3.accepterPlaceDebout();
		assertFalse(p3.estDehors());
	}
	
	@Test
	public void testEstAssis() {
		p1.accepterSortie();
		assertFalse(p1.estAssis());
		
		p2.accepterPlaceAssise();
		assertTrue(p2.estAssis());
		
		p3.accepterPlaceDebout();
		assertFalse(p3.estAssis());
	}

	@Test
	public void testEstDebout() {
		p1.accepterSortie();
		assertFalse(p1.estDebout());
		
		p2.accepterPlaceAssise();
		assertFalse(p2.estDebout());
		
		p3.accepterPlaceDebout();
		assertTrue(p3.estDebout());
	}
	
	
	@Test
	public void testMonterDans() {
		
		try {
			p1.monterDans(b1);
			p1.monterDans(b2);
			p1.monterDans(b3);
			p1.monterDans(b4);
			
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(p1.estAssis());
		assertTrue(p2.estDehors());
		assertFalse(p3.estDebout());
		assertTrue(p4.estDehors());
	}
	
	@Test
	public void testMonterDansBus1() {
		
		try {
			p1.monterDans(b1);
			p2.monterDans(b1);
			p3.monterDans(b1);
			p4.monterDans(b1);
			
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(p1.estAssis());
		assertFalse(p2.estDehors());
		assertTrue(p3.estDebout());
		assertTrue(p4.estDehors());
	}
	
	@Test
	public void testMonterDansBus2() {
		
		try {
			p1.monterDans(b2);
			p2.monterDans(b2);
			p3.monterDans(b2);
			p4.monterDans(b2);
			
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertFalse(p1.estAssis());
		assertTrue(p2.estDehors());
		assertFalse(p3.estDebout());
		assertTrue(p4.estDehors());
	}
	
	@Test
	public void testMonterDansBus3() {
		
		try {
			p1.monterDans(b3);
			p2.monterDans(b3);
			p3.monterDans(b3);
			p4.monterDans(b3);
			
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertFalse(p1.estAssis());
		assertTrue(p2.estDehors());
		assertFalse(p3.estDebout());
		assertTrue(p4.estDehors());
	}
	
	@Test
	public void testMonterDansBus4() {
		
		try {
			p1.monterDans(b4);
			p2.monterDans(b4);
			p3.monterDans(b4);
			p4.monterDans(b4);
			
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(p1.estAssis());
		assertTrue(p2.estDehors());
		assertFalse(p3.estDebout());
		assertTrue(p4.estDehors());
	}
	
	@Test
	public void testNouvelArret() throws UsagerInvalideException {
		try {
			p1.monterDans(b5);
			p2.monterDans(b5);
			p3.monterDans(b5);
			p4.monterDans(b5);
			
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(p1.estAssis());
		assertTrue(p2.estAssis());
		assertTrue(p3.estAssis());
		assertTrue(p4.estAssis());
		
		b5.allerArretSuivant();
		
		assertTrue(p1.estDehors());
		assertTrue(p2.estAssis());
		assertTrue(p3.estAssis());
		assertTrue(p4.estAssis());
		
		b5.allerArretSuivant();
		
		assertTrue(p1.estDehors());
		assertTrue(p2.estDehors());
		assertTrue(p3.estAssis());
		assertTrue(p4.estAssis());
		
		b5.allerArretSuivant();
		
		assertTrue(p1.estDehors());
		assertTrue(p2.estDehors());
		assertTrue(p3.estDehors());
		assertTrue(p4.estAssis());
		
		b5.allerArretSuivant();
		
		assertTrue(p1.estDehors());
		assertTrue(p2.estDehors());
		assertTrue(p3.estDehors());
		assertFalse(p4.estDehors());
	
		}
		
	}
	
	
	

