package tec;

import etatPassager.EtatPassager;
import etatPassager.EtatPassager.Etat;

public class PassagerStandard implements Passager, Usager {
	
	private String nom;
	private int destination;
	private Etat unEtatPassager;

	public PassagerStandard(String nom, int destination) {
		this.nom = nom;
		this.destination = destination;
		this.unEtatPassager = Etat.DEHORS;
	}
	@Override
	public void monterDans(Transport t) throws UsagerInvalideException {
		// TODO Auto-generated method stub
		if(this.estDehors()) {
			Autobus unAutobus = (Autobus) t;
			if(unAutobus.aPlaceAssise()) { unAutobus.demanderPlaceAssise(this);}
			else if(unAutobus.aPlaceDebout()) { unAutobus.demanderPlaceDebout(this);}
			else {unAutobus.demanderSortie(this);}
		}	
	}

	@Override
	public String nom() {
		// TODO Auto-generated method stub
		return this.nom;
	}

	@Override
	public boolean estDehors() {
		// TODO Auto-generated method stub
		return this.unEtatPassager == Etat.DEHORS;
	}

	@Override
	public boolean estAssis() {
		// TODO Auto-generated method stub
		return this.unEtatPassager == Etat.ASSIS;
	}

	@Override
	public boolean estDebout() {
		// TODO Auto-generated method stub
		return this.unEtatPassager == Etat.DEBOUT;
	}

	@Override
	public void accepterSortie() {
		// TODO Auto-generated method stub
		this.unEtatPassager = Etat.DEHORS;
		
	}

	@Override
	public void accepterPlaceAssise() {
		// TODO Auto-generated method stub
		this.unEtatPassager = Etat.ASSIS;
		
	}

	@Override
	public void accepterPlaceDebout() {
		// TODO Auto-generated method stub
		this.unEtatPassager = Etat.DEBOUT;
		
	}

	@Override
	public void nouvelArret(Bus bus, int numeroArret) {
		// TODO Auto-generated method stub
		if(numeroArret == this.destination) {
			this.accepterSortie();
			
		}

	}
	
	public String toString() {
		return this.nom + " " + this.unEtatPassager;
	}

}
