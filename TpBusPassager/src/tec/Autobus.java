package tec;

import java.util.ArrayList;

import Jauge.JaugeNaturel;

public class Autobus implements Bus, Transport {
	
	private JaugeNaturel placeAssis;
	private JaugeNaturel placeDebout;
	private ArrayList<PassagerStandard> listPassager;
	private int arret;
	

	public Autobus(int placeAssis, int placeDebout) {
		this.placeAssis = new JaugeNaturel(-1, placeAssis, 0);
		this.placeDebout = new JaugeNaturel(-1, placeDebout, 0);
		this.listPassager = new ArrayList<PassagerStandard>();
		this.arret = 0;
	}

	@Override
	public boolean aPlaceAssise() {
		return this.placeAssis.estVert();
	}

	@Override
	public boolean aPlaceDebout() {
		return this.placeDebout.estVert();
	}


	@Override
	public void demanderPlaceAssise(Passager p) {
		// TODO Auto-generated method stub
		if (this.aPlaceAssise() && p.estDehors()) {
			p.accepterPlaceAssise();
			this.placeAssis.incrementer();
			this.listPassager.add((PassagerStandard)p);
		}
		
	}

	@Override
	public void demanderPlaceDebout(Passager p) {
		// TODO Auto-generated method stub
		if (this.aPlaceDebout() && p.estDehors()) {
			System.out.println("ok");
			p.accepterPlaceDebout();
			this.placeDebout.incrementer();
			this.listPassager.add((PassagerStandard)p);
		}
	}

	@Override
	public void demanderChangerEnDebout(Passager p) {
		// TODO Auto-generated method stub
		if (this.aPlaceDebout() && p.estAssis()) {
			p.accepterPlaceDebout();
			this.placeAssis.decrementer();
			this.placeDebout.incrementer();
		}
	}

	@Override
	public void demanderChangerEnAssis(Passager p) {
		// TODO Auto-generated method stub
		if (this.aPlaceAssise() && p.estDebout()) {
			p.accepterPlaceAssise();
			this.placeAssis.incrementer();
			this.placeDebout.decrementer();
		}
	}

	@Override
	public void demanderSortie(Passager p) {
		// TODO Auto-generated method stub
		if(p.estAssis()) {
			this.placeAssis.decrementer();
		}else if (p.estDebout()) {
			this.placeDebout.decrementer();
		}
		p.accepterSortie();
		this.listPassager.remove((PassagerStandard)p);
	}

	@Override
	public void allerArretSuivant() throws UsagerInvalideException {
		// TODO Auto-generated method stub
		this.arret++;
		for (PassagerStandard passager : this.listPassager) {
			passager.nouvelArret(this, this.arret);
		}
	}

	@Override
	public String toString() {
		int assis = 0;
		int debout = 0;
		for (PassagerStandard passager : this.listPassager) {
			System.out.println("aza"+passager.estDebout());
			if (passager.estAssis()) {
				assis++;
			}else if (passager.estDebout()) {
				debout++;
			}
		}
		return "[arret:"+this.arret+", assis:"+assis+", debout:"+debout+"]";
	}

	
	
}
