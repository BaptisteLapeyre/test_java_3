package tec;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AutobusTest {

	Autobus b1;
	Autobus b2;
	Autobus b3;
	Autobus b4;
	Autobus b5;
	
	PassagerStandard p1;
	PassagerStandard p2;
	PassagerStandard p3;
	PassagerStandard p4;
	
	@Before
	public void setUp() throws Exception {
		b1 = new Autobus(1, 2);
	    b2 = new Autobus(0, 0);
	    b3 = new Autobus(0, 1);
	    b4 = new Autobus(1, 0);
	    b5 = new Autobus(25, 25);
	     
	    p1 = new PassagerStandard("Michel", 1);
	    p2 = new PassagerStandard("Bernadette", 2);
	    p3 = new PassagerStandard("Hector", 3);
	    p4 = new PassagerStandard("Flanders", 5);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAPlaceAssis() {
		assertTrue(b1.aPlaceAssise());
		assertFalse(b2.aPlaceAssise());
	}
	
	@Test
	public void testAPlacedebout() {
		assertTrue(b1.aPlaceDebout());
		assertFalse(b2.aPlaceDebout());
	}
	
	@Test
	public void testToString() {
		assertEquals(b2.toString(),"[arret:0, assis:0, debout:0]");
	}
	
	@Test
	public void testMonter() {
		try {
			p1.monterDans(b1);
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			fail("fail monter dans autobus assis");
		}
		assertEquals(b1.toString(),"[arret:0, assis:1, debout:0]");
		try {
			p2.monterDans(b1);
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			fail("fail monter dans autobus debout");
		}
		assertEquals(b1.toString(),"[arret:0, assis:1, debout:1]");
	}
	
	@Test
	public void testSortie() {
		try {
			p3.monterDans(b3);
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			fail("fail monter dans autobus debout");
		}
		b3.demanderSortie(p3);
		assertEquals(b1.toString(),"[arret:0, assis:0, debout:0]");
	}
	
	@Test
	public void testArretSuivant() {
		try {
			b4.allerArretSuivant();
		} catch (UsagerInvalideException e) {
			// TODO Auto-generated catch block
			fail("fail aller arret suivant");
		}
		assertEquals(b4.toString(),"[arret:1, assis:0, debout:0]");
	}

}
